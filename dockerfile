FROM python:3.7
ADD ./main.py /
ADD ./data.txt /
ADD ./requirements.txt /
RUN pip install -r requirements.txt
CMD python main.py
